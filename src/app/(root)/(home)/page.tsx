import Banner from "@/components/shared/banner/Banner";
import React from "react";
import BestSellingProducts from "./_components/best-selling-products/BestSellingProducts";

export const metadata = {
  title: "Pet Store",
};

export default function Home() {
  return (
    <div>
      <Banner />
      <BestSellingProducts />
    </div>
  );
}
