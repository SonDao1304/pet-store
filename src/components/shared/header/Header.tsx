import Logo from "./Logo";
import MenuBar from "./MenuBar";
import Navbar from "./Navbar";
import SearchBar from "./SearchBar";

export default function Header() {
  return (
    <header className="h-header flex-between px-20 gap-x-24">
      <div className="flex-center gap-x-[80px]">
        <Logo />
        <Navbar />
      </div>
      <div className="flex-center gap-x-10">
        <SearchBar />
        <MenuBar />
      </div>
    </header>
  );
}
