import React from "react";
import { Input } from "../../ui/input";
import { IoIosSearch } from "react-icons/io";

export default function SearchBar() {
  return (
    <div className="relative">
      <Input
        type="text"
        placeholder="Search products ..."
        className="w-[250px] bg-input rounded-3xl p-5 border-input focus:border-secondary focus:border-[1px] outline-none"
      />
      <span className="absolute top-1/2 -translate-y-1/2 right-2 flex-center rounded-full bg-secondary text-white p-[6px]">
        <IoIosSearch size={20} />
      </span>
    </div>
  );
}
