"use client";

import Link from "next/link";
import { usePathname } from "next/navigation";
import React from "react";

import { cn } from "@/lib/utils";

export const navLinks = [
  {
    route: "/",
    label: "Home",
  },

  {
    route: "/products",
    label: "Products",
  },
  {
    route: "/about",
    label: "About",
  },
  {
    route: "/contact-us",
    label: "Contact Us",
  },
];

export const navRoutes = navLinks.map(item => item.route);

export default function Navbar() {
  const pathname = usePathname();
  return (
    <nav className="flex-center">
      {navLinks.map(item => {
        const isActive =
          pathname === item.route || pathname.startsWith(`${item.route}/`);

        return (
          <Link
            href={item.route}
            key={item.label}
            className={cn(
              "flex gap-4 items-center p-4 rounded-lg justify-start",
              {
                "text-primary": isActive,
              }
            )}
          >
            <p className="text-lg font-medium max-lg:hidden">{item.label}</p>
          </Link>
        );
      })}
    </nav>
  );
}
