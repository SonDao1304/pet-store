import Link from "next/link";
import React from "react";
import { FiUser } from "react-icons/fi";
import { BiCart } from "react-icons/bi";
import { MdFavoriteBorder } from "react-icons/md";

const menus = [
  {
    label: "user",
    route: "/user",
    icon: <FiUser size={25} />,
  },
  {
    label: "favorite",
    route: "/favorite",
    icon: <BiCart size={25} />,
  },
  {
    label: "cart",
    route: "/cart",
    icon: <FiUser size={25} />,
  },
];
export default function MenuBar() {
  return (
    <ul className="flex gap-x-4">
      {menus.map(menu => (
        <li key={menu.label}>
          <Link href={menu.route}>{menu.icon}</Link>
        </li>
      ))}
    </ul>
  );
}
